// Fill out your copyright notice in the Description page of Project Settings.


#include "EQC_FindPlayer.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Point.h"

void UEQC_FindPlayer::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	if (AActor* playerActor = Cast<AActor>(GetWorld()->GetFirstPlayerController()->GetPawn()))
	{
		UEnvQueryItemType_Point::SetContextHelper(ContextData, playerActor->GetActorLocation());
	}
}
