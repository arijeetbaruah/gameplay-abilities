// Fill out your copyright notice in the Description page of Project Settings.


#include "BTT_FindPlayer.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

UBTT_FindPlayer::UBTT_FindPlayer(FObjectInitializer const& ObjectInitializer)
{
	NodeName = TEXT("Find Player");
}

EBTNodeResult::Type UBTT_FindPlayer::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	auto location = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();
	OwnerComp.GetBlackboardComponent()->SetValueAsVector(GetSelectedBlackboardKey(), location);

	return EBTNodeResult::Succeeded;
}
