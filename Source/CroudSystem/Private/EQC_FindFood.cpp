// Fill out your copyright notice in the Description page of Project Settings.


#include "EQC_FindFood.h"

#include "EnvironmentQuery/EnvQueryTypes.h"
#include "Food.h"
#include "Kismet/GameplayStatics.h"
#include "EnvironmentQuery/Items/EnvQueryItemType_Actor.h"

void UEQC_FindFood::ProvideContext(FEnvQueryInstance& QueryInstance, FEnvQueryContextData& ContextData) const
{
	TArray<AActor*> FoodActors;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AFood::StaticClass(), FoodActors);
	FoodActors.Remove(nullptr);

	if (FoodActors.Num() > 0)
	{
		UEnvQueryItemType_Actor::SetContextHelper(ContextData, FoodActors);
	}
}
