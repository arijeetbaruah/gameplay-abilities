// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "NPC_AIController.generated.h"

/**
 *
 */
UCLASS()
class CROUDSYSTEM_API ANPC_AIController : public AAIController
{
	GENERATED_BODY()

protected:
	float hunger;
	class AAICharacter* aiCharacter;

public:
	virtual void OnPossess(APawn* const InPawn) override;
	virtual void Tick(float const DeltaTime) override;

	float GetHunger() const;
	void SetHunger(float const newHunger);

	bool IsHungry();
};
